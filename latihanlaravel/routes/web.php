<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('tugaslaravel.home');
});

Route::get('/register', function () {
    return view('tugaslaravel.register');
});

Route::get('/welcome', function () {
    return view('tugaslaravel.welcome2');
});

Route::get('/data-table', function () {
    return view('table.data-table');

});

Route::get('/table', function () {
    return view('table.table');

});

// CRUD cast
Route ::get('/cast/create', 'castcontroller@create');
Route ::post('cast/', 'castcontroller@store');
Route ::get('/cast', 'castcontroller@index');
Route ::get('/cast/{cast_id}', 'castcontroller@show');
Route ::get('/cast/{cast_id}/edit', 'castcontroller@edit');
Route ::put('/cast/{cast_id}', 'castcontroller@update');
Route ::delete('/cast/{cast_id}', 'castcontroller@destroy');